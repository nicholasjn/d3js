// D3 basic, how to make style (color, etc)
// Use select method to select which component we want to process.
// d3.select('h1').style('color', 'red');

// d3.select('body').append('p').text('First paragraph');
// d3.select('p').style('color', 'blue');

/*------------------------------------------------------------------------------------*/
// Create a simple bar chart
var dataset = [80, 100, 56, 120, 180, 30, 40, 120, 160];

// This section is to initialize variable
// Svg is our workspace, so the bar chart will be generated on svg area.
// svgWidth is width of our workspace, and svgHeight is the height
// barPadding is to specified space are around bar area, here the width is 5 pixel. 
var svgWidth = 500, svgHeight = 300, barPadding = 10;

// The barWidth (width of each bar) can be obtained from svg width area
// divided by number of the bar we want to created.
var barWidth = (svgWidth / dataset.length);

// This section, we assign the value of the width and height of svg into svg object.
var svg = d3.select('svg')
    .attr("width", svgWidth)
    .attr("height", svgHeight);
    
// This section is the creation process of the bar chart
// From svg object we select rectangle shape
var barChart = svg.selectAll("rect")

// Then we specified our dataset into data method
    .data(dataset)

// Then we call enter method, this will process each element of data in array one by one. Similar like for each.
    .enter()
    
// Then we append the rect. Not sure why use append
    .append("rect")
    
// Then we specified the y-axis attribute. x-axis and y-axis is the anchor point where our chart will be created.
// So the rect will be created from the top, continue to the bottom until the lowest point of svg area (y equal svg height).
    .attr("y", function(d) {
         return svgHeight - d  
    })
    
// Then we specified the height of the bar, determined by the value of the data (element of array)
    .attr("height", function(d) { 
        return d; 
    })
	
// The bar width is specified by substraction between barWidth and bar padding. Increase barPadding to make the space between the bar larger.
    .attr("width", barWidth - barPadding)
    
// For transform attribut, not sure how this works.
    .attr("transform", function (d, i) {
        var translate = [barWidth * i, 0]; 
        return "translate("+ translate +")";
    });

var label = svg.selectAll('text')
	.data(dataset)
	.enter()
	.append('text')
	.text(function(d){
		return d;
	})
	.attr('y', function(d, i){
		return svgHeight - d - 2;
	})
	.attr('x', function(d, i){
		return (barWidth * i) + ((barWidth - barPadding - d.toString().length)/4);
	})
	.attr('fill', '#A64C38');


/* How to scale the bar chart.
// To adjust the scale, just add variable to svgHeight. E.q. .range([0, svgHeight - 100])
var yScale = d3.scaleLinear()
    .domain([0, d3.max(dataset)])
    .range([0, svgHeight]);
*/